package main

import (
	"fmt"
	"log"
	"sync"
)

type Downloader struct {
}

func (d *Downloader) Run1() <-chan string {

	var wg sync.WaitGroup
	wg.Add(numWorkers)

	in := make(chan string)
	out := make(chan string)

	dl := func(taskId int, inputStream <-chan string) {
		defer wg.Done()
		for message := range inputStream {
			log.Printf("Downloader worker %d received a mesage: %s", taskId, message)
			out <- message
		}
	}

	go func() {

		for i := 0; i < numWorkers; i++ {
			log.Printf("Adding Downloader.worker %d", i)
			go dl(i, in)
		}

		for i := 1; i <= 3000; i++ {
			in <- fmt.Sprintf("This is item %d", i)
		}
	}()

	return out

}

func (d *Downloader) Run() <-chan string {

	var wg sync.WaitGroup
	wg.Add(1)

	out := make(chan string)

	go func() {

		for i := 1; i <= 3000; i++ {
			message := fmt.Sprintf("This is item %d", i)
			log.Printf("Downloader, message: %s", message)
			out <- message
		}
		wg.Done()
	}()

	go func() {
		wg.Wait()
		// close(out)
	}()

	return out

}
