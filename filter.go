package main

import (
	"log"
	"sync"
)

type Filterer struct {
}

func (f *Filterer) Run(inputStream <-chan string) <-chan string {

	var wg sync.WaitGroup
	wg.Add(numWorkers)

	out := make(chan string)

	dl := func(taskId int, inputStream <-chan string) {
		defer wg.Done()
		for message := range inputStream {
			log.Printf("Filterer worker %d received a mesage: %s", taskId, message)
			out <- message
		}
	}

	go func() {
		for i := 0; i < numWorkers; i++ {
			log.Printf("Adding Filterer.worker %d", i)
			go dl(i, inputStream)
		}
	}()

	go func() {
		wg.Wait()
		// close(out)
	}()

	return out

}
