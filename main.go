package main

const (
	numWorkers = 1000
)

func main() {
	dlr := &Downloader{}
	flt := &Filterer{}
	svr := &Saver{}

	downloaded := dlr.Run1()
	filtered := flt.Run(downloaded)
	<-svr.Run(filtered)
}
